package id.ac.ui.cs.advprog.tutorial3.facade.core;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.AnotherTransformation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TransformationFacadeTest {
    private Class<?> facadeClass;

    @BeforeEach
    public void setUp() throws Exception {
        facadeClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.TransformationFacade"
        );
    }

    @Test
    public void testFacadeHasEncodeMethod() throws Exception {
        Method translate = facadeClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testFacadeEncodesCorrectly() {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "AVA$xqyZCCn(v%C+q>MvbH/Ad%n;pMpn@$@V$x}ZA/vddXs*DMFw";

        Spell result = new TransformationFacade().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testFacadeHasDecodeMethod() throws Exception {
        Method translate = facadeClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testFacadeDecodesCorrectly() {
        String text = "AVA$xqyZCCn(v%C+q>MvbH/Ad%n;pMpn@$@V$x}ZA/vddXs*DMFw";
        Codex codex = RunicCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new TransformationFacade().decode(spell);
        assertEquals(expected, result.getText());
    }
}
