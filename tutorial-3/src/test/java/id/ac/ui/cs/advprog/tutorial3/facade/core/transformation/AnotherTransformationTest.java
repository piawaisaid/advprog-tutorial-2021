package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static  org.junit.jupiter.api.Assertions.assertTrue;

public class AnotherTransformationTest {
    private Class<?> anotherClass;

    @BeforeEach
    public void setUp() throws Exception {
        anotherClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.AnotherTransformation"
        );
    }

    @Test
    public void testAnotherHasEncodeMethod() throws Exception {
        Method translate = anotherClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testAnotherEncodesCorrectly() {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "MUZclU4UhX4C4qYhn4ni4U4VfUWemgcnb4ni4ZilaY4iol4mqilX";

        Spell result = new AnotherTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testAnotherEncodesCorrectlyWithCustomKey() {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "NVadmV5ViY5D5rZio5oj5V5WgVXfnhdoc5oj5ajmbZ5jpm5nrjmY";

        Spell result = new AnotherTransformation(5).encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testAnotherHasDecodeMethod() throws Exception {
        Method translate = anotherClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testAnotherDecodesCorrectly() {
        String text = "MUZclU4UhX4C4qYhn4ni4U4VfUWemgcnb4ni4ZilaY4iol4mqilX";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new AnotherTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testAnotherDecodesCorrectlyWithCustomKey() {
        String text = "NVadmV5ViY5D5rZio5oj5V5WgVXfnhdoc5oj5ajmbZ5jpm5nrjmY";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new AnotherTransformation(5).decode(spell);
        assertEquals(expected, result.getText());
    }
}
