package id.ac.ui.cs.advprog.tutorial3.facade.core;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.*;

import java.util.ArrayList;
import java.util.List;

public class TransformationFacade {
    private List<Transformation> transformations;

    public TransformationFacade(List<Transformation> transformations) {
        this.transformations = transformations;
    }

    public TransformationFacade() {
        this(new ArrayList<Transformation>() {{
            add(new AbyssalTransformation());
            add(new AnotherTransformation());
            add(new CelestialTransformation());
        }});
    }

    public Spell encode(Spell spell) {
        for (int i = 0; i < transformations.size(); i++) {
            spell = transformations.get(i).encode(spell);
        }

        return CodexTranslator.translate(spell, RunicCodex.getInstance());
    }

    public Spell decode(Spell spell) {
        for (int i = transformations.size() - 1; i >= 0; i--) {
            spell = transformations.get(i).decode(spell);
        }

        return CodexTranslator.translate(spell, AlphaCodex.getInstance());
    }
}
