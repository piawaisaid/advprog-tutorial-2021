package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class AnotherTransformation implements Transformation {
    private int key;

    public AnotherTransformation(int key) {
        this.key = key;
    }

    public AnotherTransformation() {
        this(69);
    }

    public Spell encode(Spell spell) {
        return process(spell, true);
    }

    public Spell decode(Spell spell) {
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode) {
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int selector = encode ? -1 : 1;
        int n = text.length();
        int m = codex.getCharSize();
        char[] res = new char[n];
        for (int i = 0; i < n; i++) {
            int newIdx = (codex.getIndex(text.charAt(i)) + key * selector) % m;
            newIdx = newIdx < 0 ? m + newIdx : newIdx;
            res[i] = codex.getChar(newIdx);
        }

        return new Spell(new String(res), codex);
    }
}
