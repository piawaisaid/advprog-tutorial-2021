package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/logs")
public class LogController {
    @Autowired
    private LogService logService;

    @PostMapping(path = "/mahasiswa/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity postLog(@PathVariable(value = "npm") String npm, @RequestBody Log log) {
        return ResponseEntity.ok(logService.createLog(log, npm));
    }

    @GetMapping(path = "/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getLogById(@PathVariable(value = "id") String id) {
        return ResponseEntity.ok(logService.getLogById(id));
    }

    @PutMapping(path = "/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity updateLog(@RequestBody Log log, @PathVariable(value = "id") String id) {
        return ResponseEntity.ok(logService.updateLog(log, id));
    }

    @DeleteMapping(path = "/{id}", produces = {"application/json"})
    public ResponseEntity deleteLog(@PathVariable(value = "id") String id) {
        logService.deleteLog(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @GetMapping(path = "/mahasiswa/{npm}", produces = {"application/json"})
    public ResponseEntity getLogsMahasiswa(@PathVariable(value = "npm") String npm, @RequestParam(required = false) String month) {
        return ResponseEntity.ok(logService.getListLogsByNPM(npm, month));
    }

    @GetMapping(path = "/mahasiswa/{npm}/reports", produces = {"application/json"})
    public ResponseEntity getLogsReportMahasiswa(@PathVariable(value = "npm") String npm, @RequestParam(required = false) String month, @RequestParam(required = false) String year) {
        return ResponseEntity.ok(logService.getLogsSummaryByNPM(npm, month, year));
    }
}
