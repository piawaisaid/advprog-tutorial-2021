package csui.advpro2021.tais.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.text.DateFormatSymbols;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LogSummary {
    private String bulan;
    private int tahun;
    private double jamKerja;
    private double pembayaran;

    public LogSummary(int bulan, int tahun, double jamKerja) {
        this.bulan = new DateFormatSymbols().getMonths()[bulan - 1];
        this.tahun = tahun;
        this.jamKerja = jamKerja;
        this.pembayaran = jamKerja * 350;
    }
}
