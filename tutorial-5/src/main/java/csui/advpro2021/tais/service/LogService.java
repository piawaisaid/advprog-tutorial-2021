package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;

public interface LogService {
    Log createLog(Log log, String npm);
    Log getLogById(String id);
    Log updateLog(Log log, String id);
    void deleteLog(String id);
    Iterable<Log> getListLogsByNPM(String npm, String month);
    Iterable<LogSummary> getLogsSummaryByNPM(String npm, String month, String year);
}
