package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
    //ToDo: Complete me
    @Override
    public String defend() {
        return "Your attacks can't get through my shield";
    }

    @Override
    public String getType() {
        return "Shield";
    }
}
