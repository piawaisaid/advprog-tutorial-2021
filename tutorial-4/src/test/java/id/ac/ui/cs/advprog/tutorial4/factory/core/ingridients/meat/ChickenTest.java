package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ChickenTest {
    private Class<?> chickenClass;
    private Meat chicken;

    @BeforeEach
    public void setUp() throws Exception {
        chickenClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken");
        chicken = new Chicken();
    }

    @Test
    public void testChickenIsAConcreteClass() {
        assertFalse(Modifier.isAbstract(chickenClass.getModifiers()));
    }

    @Test
    public void testChickenIsAMeat() {
        Collection<Type> interfaces = Arrays.asList(chickenClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat")
                )
        );
    }

    @Test
    public void testChickenOverridesGetDescriptionMethod() throws Exception {
        Method getDescription = chickenClass.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String", getDescription.getGenericReturnType().getTypeName());
        assertEquals(0, getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(getDescription.getModifiers()));
    }

    @Test
    public void testGetDescriptionMethodIsImplementedCorrectly() {
        assertEquals("Adding Wintervale Chicken Meat...", chicken.getDescription());
    }
}
