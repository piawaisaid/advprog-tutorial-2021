package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MondoUdonFactoryTest {
    private Class<?> mondoUdonFactoryClass;
    private MenuFactory mondoUdonFactory;

    @BeforeEach
    public void setUp() throws Exception {
        mondoUdonFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MondoUdonFactory");
        mondoUdonFactory = new MondoUdonFactory();
    }

    @Test
    public void testMondoUdonFactoryIsAConcreteClass() {
        assertFalse(Modifier.isAbstract(mondoUdonFactoryClass.getModifiers()));
    }

    @Test
    public void testMondoUdonFactoryIsAMenuFactory() {
        Collection<Type> interfaces = Arrays.asList(mondoUdonFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MenuFactory")
                )
        );
    }

    @Test
    public void testMondoUdonFactoryOverridesCreateNoodleMethod() throws Exception {
        Method createNoodle = mondoUdonFactoryClass.getDeclaredMethod("createNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle", createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0, createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(createNoodle.getModifiers()));
    }

    @Test
    public void testMondoUdonFactoryOverridesCreateMeatMethod() throws Exception {
        Method createMeat = mondoUdonFactoryClass.getDeclaredMethod("createMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat", createMeat.getGenericReturnType().getTypeName());
        assertEquals(0, createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(createMeat.getModifiers()));
    }

    @Test
    public void testMondoUdonFactoryOverridesCreateToppingMethod() throws Exception {
        Method createTopping = mondoUdonFactoryClass.getDeclaredMethod("createTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping", createTopping.getGenericReturnType().getTypeName());
        assertEquals(0, createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(createTopping.getModifiers()));
    }

    @Test
    public void testMondoUdonFactoryOverridesCreateFlavorMethod() throws Exception {
        Method createFlavor = mondoUdonFactoryClass.getDeclaredMethod("createFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor", createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0, createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(createFlavor.getModifiers()));
    }

    @Test
    public void testCreateNoodleMethodIsCorrectlyImplemented() {
        assertTrue(mondoUdonFactory.createNoodle() instanceof Udon);
    }

    @Test
    public void testCreateMeatMethodIsCorrectlyImplemented() {
        assertTrue(mondoUdonFactory.createMeat() instanceof Chicken);
    }

    @Test
    public void testCreateToppingMethodIsCorrectlyImplemented() {
        assertTrue(mondoUdonFactory.createTopping() instanceof Cheese);
    }

    @Test
    public void testCreateFlavorMethodIsCorrectlyImplemented() {
        assertTrue(mondoUdonFactory.createFlavor() instanceof Salty);
    }
}
