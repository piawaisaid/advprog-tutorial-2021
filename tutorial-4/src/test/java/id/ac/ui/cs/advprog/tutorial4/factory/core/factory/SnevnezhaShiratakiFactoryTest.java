package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SnevnezhaShiratakiFactoryTest {
    private Class<?> snevnezhaShiratakiFactoryClass;
    private MenuFactory snevnezhaShiratakiFactory;

    @BeforeEach
    public void setUp() throws Exception {
        snevnezhaShiratakiFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.SnevnezhaShiratakiFactory");
        snevnezhaShiratakiFactory = new SnevnezhaShiratakiFactory();
    }

    @Test
    public void testSnevnezhaShiratakiFactoryIsAConcreteClass() {
        assertFalse(Modifier.isAbstract(snevnezhaShiratakiFactoryClass.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiFactoryIsAMenuFactory() {
        Collection<Type> interfaces = Arrays.asList(snevnezhaShiratakiFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MenuFactory")
                )
        );
    }

    @Test
    public void testSnevnezhaShiratakiFactoryOverridesCreateNoodleMethod() throws Exception {
        Method createNoodle = snevnezhaShiratakiFactoryClass.getDeclaredMethod("createNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle", createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0, createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(createNoodle.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiFactoryOverridesCreateMeatMethod() throws Exception {
        Method createMeat = snevnezhaShiratakiFactoryClass.getDeclaredMethod("createMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat", createMeat.getGenericReturnType().getTypeName());
        assertEquals(0, createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(createMeat.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiFactoryOverridesCreateToppingMethod() throws Exception {
        Method createTopping = snevnezhaShiratakiFactoryClass.getDeclaredMethod("createTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping", createTopping.getGenericReturnType().getTypeName());
        assertEquals(0, createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(createTopping.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiFactoryOverridesCreateFlavorMethod() throws Exception {
        Method createFlavor = snevnezhaShiratakiFactoryClass.getDeclaredMethod("createFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor", createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0, createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(createFlavor.getModifiers()));
    }

    @Test
    public void testCreateNoodleMethodIsCorrectlyImplemented() {
        assertTrue(snevnezhaShiratakiFactory.createNoodle() instanceof Shirataki);
    }

    @Test
    public void testCreateMeatMethodIsCorrectlyImplemented() {
        assertTrue(snevnezhaShiratakiFactory.createMeat() instanceof Fish);
    }

    @Test
    public void testCreateToppingMethodIsCorrectlyImplemented() {
        assertTrue(snevnezhaShiratakiFactory.createTopping() instanceof Flower);
    }

    @Test
    public void testCreateFlavorMethodIsCorrectlyImplemented() {
        assertTrue(snevnezhaShiratakiFactory.createFlavor() instanceof Umami);
    }
}
